(function ($) {
  Drupal.behaviors.calendarButton = {
    attach: function (context, settings) {
      var CalendarButton = document.getElementById('hide-menu-title');
      var HideMenu = document.getElementById('hide-menu');
      CalendarButton.onclick = function () {
        if (HideMenu.style.display === "block") {
          HideMenu.style.display = "none";
        }
        else {
          HideMenu.style.display = "block";
        }
      };
    }
  };


  Drupal.behaviors.mainMenuMobile = {
    attach: function (context, settings) {
      var MenuMobileButton = document.getElementById('block-block-5');
      var MainMenu = document.getElementById('navigation');
      var symbol = document.querySelectorAll('#block-block-5 i');
      MenuMobileButton.onclick = function () {
        var str = $("#navigation").css("left").slice(0, -2);
        var int = parseInt(str);
        if (int < 0) {
          $("#block-block-5 i").removeClass('fa-bars');
          $("#block-block-5").css("transform", "rotate(90deg)");
          $("#block-block-5 i").addClass('fa-arrow-down');
          $("#navigation").css("left", "0%");
        }
        else {
          $("#navigation").css("left", "-68%");
          $("#block-block-5 i").removeClass('fa-arrow-down');
          $("#block-block-5").css("transform", "rotate(0deg)");
          $("#block-block-5 i").addClass('fa-bars');
        }
      };
    }
  };

  Drupal.behaviors.headerBlockMobile = {
    attach: function (context, settings) {
      var headerBlockButton = document.getElementById('block-block-6');
      var headerBlock = document.getElementById('block-block-3');
      headerBlockButton.onclick = function () {
        if ($("#block-block-3").css("opacity") === "0") {
          $("#block-block-3").css("opacity", "1");
          $("#block-block-3").css("display", "block");
        }
        else {
          $("#block-block-3").css("opacity", "0");
          $("#block-block-3").css("display", "none");
        }
      };
    }
  };

  Drupal.behaviors.scrollMenu = {
    attach: function (context, settings) {
      $("#main-menu").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href');
        if (id.charAt(0) === '/') {
          id = id.slice(1);
        }
        var top = $(id).offset().top;
        top = top - 30;
        $('body,html').animate({scrollTop: top}, 1500);
      });
    }
  };

  Drupal.behaviors.sitePreloader = {
    attach: function (context, settings) {
      $(document).ready(function () {
        setTimeout(function () {
          $('#page-loader').addClass('loaded');
        }, 1000);

      });
    }
  };
}(jQuery));

/*function initMap() {
  var myLatLng = {lat: 50.455863, lng: 30.487902};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
  marker.setMap(map);
}*/