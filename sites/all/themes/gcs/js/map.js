function initMap(){
  var coordinates = {lat: 50.455863, lng: 30.487902},
      map = new google.maps.Map(document.getElementById('map'), {
        center: coordinates,
        disableDefaultUI: 0
      }),
      image = 'sites/all/themes/gcs/images/marker.png',
      marker = new google.maps.Marker({
        position: coordinates,
        map: map,
        icon: image
      });
}