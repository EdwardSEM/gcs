<div id="paragraph-bundle-<?php print $elements['#bundle']; ?>"
     class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <div class="content"<?php print $content_attributes; ?>>
      <?php print render($content['field_plain_title']);
      print render($content['field_about_text']);
      print render($content['field_links']);
      ?>
      <?php if (!empty($content['field_sub_links'])): ?>
          <div class="hide-menu-wrapper">
              <span id="hide-menu-title">ADD TO CALENDAR</span>
              <ul id="hide-menu" style="display: none;">
                <?php print render($content['field_sub_links']); ?>
              </ul>
          </div>
      <?php endif; ?>
    </div>
</div>

